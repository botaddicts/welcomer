# 👋 Welcomer bot
![demo of the result](./demo.png)

A bot that says who joins and leave the server with a message and an image.

## Dependencies
* `python3`
* `systemd` (for running it on server)
* A bot with all intents activated (token)
* The ID of the channel where the messages will be sent (channel_id)

## Setup
1. Clone the repository

```bash
git clone https://codeberg.org/botaddicts/welcomer.git
cd welcomer/
```

2. Install nextcord

```bash
pip install -r requirements.txt
```

3. Copy and customize the config file

```bash
cp config.template.py config.py
nano config.py
```

4. Run the bot

```bash
python3 main.py
```

5. Setup systemd

```bash
sed -i "s|ABSOLUTE_PATH|$(pwd)|g" welcomer.service
sed -i "s|USER|$USER|g" welcomer.service
sudo cp welcomer.service /etc/systemd/system/
sudo systemctl start welcomer
```

## Usage
There's nothing to do, the bot will just send the messages when someone joins or leave the server.

If you want to try it out, create a new bot, invite it to the server, then kick it.
