import nextcord
from config import token, channel_id, description_join, description_left, image_join, image_left, title_join, title_left

import logging
logging.basicConfig(level=logging.INFO)

intents = nextcord.Intents.default()
intents.members = True
client = nextcord.Client(intents=intents)

@client.event
async def on_member_join(member):
    channel = client.get_channel(channel_id)

    colour = nextcord.Colour.green()
    embed = nextcord.Embed(colour=colour, title=title_join.format(**locals()), description=description_join.format(**locals()))
    embed.set_image(url=image_join)
    await channel.send(embed=embed)

@client.event
async def on_member_remove(member):
    channel = client.get_channel(channel_id)

    colour = nextcord.Colour.red()
    embed = nextcord.Embed(colour=colour, title=title_left.format(**locals()), description=description_left.format(**locals()))
    embed.set_image(url=image_left)
    await channel.send(embed=embed)

client.run(token)
